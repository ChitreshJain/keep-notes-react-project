import React, { Component } from "react";
import "./App.css";

var i = 0;
var list = [];
var matchId;
class App extends Component {
  constructor() {
    super();
  document.querySelector('body').addEventListener('click', function (event) {
      if (event.target.tagName.toLowerCase() === 'textarea') {
        event.target.attributes.cols.value = "30";
        event.target.attributes.rows.value = "12";
        var ids = event.target.attributes.class.value;
        matchId = event.target.attributes.class.value;
        for (let i = 0; i < document.getElementsByTagName("button").length; i++) {
          if (document.getElementsByTagName("button")[i].attributes.class.value === ids) {
            document.getElementsByTagName("button")[i].setAttribute("style", "margin-left:-145px;");
          }
        }
      }
      else if (event.target.tagName.toLowerCase() === 'button') {
        console.log(event.target.attributes.class.value + " is its class name")
        var elem = event.target.attributes.class.value;
        console.log("elem type " + typeof (elem) + "   value " + elem)
        if (event.target.attributes.class.value === "but") {
          console.log("You pressed the main button")
          console.log(JSON.stringify(list));
          list.sort((obj1, obj2)=>{return obj2.dateTime - obj1.dateTime})
          console.log(JSON.stringify(list));
          for(var k=0; k< list.length; k++){
            document.getElementsByTagName("TEXTAREA")[k].value = list[k].description;
          }
        }
        else {
          for (let i = 0; i < document.getElementsByTagName("TEXTAREA").length; i++) {
            if (document.getElementsByTagName("TEXTAREA")[i].attributes.class.value === elem)
              document.getElementsByTagName("TEXTAREA")[i].remove();
          }
          for (let i = 0; i < document.getElementsByTagName("button").length; i++) {
            if (document.getElementsByTagName("button")[i].attributes.class.value === elem)
              document.getElementsByTagName("button")[i].remove()
          }
          i--;
          list.splice(i, 1);
        }
      }
      else {
        if (document.getElementsByTagName("TEXTAREA")) {
          for (let key in document.getElementsByTagName("TEXTAREA")) {
            if (document.getElementsByTagName("TEXTAREA")[key].setAttribute !== undefined) {
              console.log("ids" + typeof(matchId));
              for(let m = 0; m< list.length; m++){
                  if(key === matchId){
                    console.log("inside")
                  list[matchId].description = document.getElementsByTagName("TEXTAREA")[key].value;
                  list[matchId].dateTime = Date.parse(new Date().toLocaleDateString() + " " + new Date().toLocaleTimeString());
                  }
              }
              document.getElementsByTagName("TEXTAREA")[key].setAttribute("cols", "20");
              document.getElementsByTagName("TEXTAREA")[key].setAttribute("rows", "8");
              // document.getElementsByTagName("button")[key].setAttribute("style", "height: 140px;");
            }
          }
        }
        if (document.getElementsByTagName("button")) {
          for (let key in document.getElementsByTagName("button")) {
            if (document.getElementsByTagName("button")[key].setAttribute !== undefined) {
              if (document.getElementsByTagName("button")[key].attributes.class.value !== "but") {
                document.getElementsByTagName("button")[key].setAttribute("style", "margin-left: -110px;");
              }
            }
          }
        }
        console.log(JSON.stringify(list))
      }

  });
  }
  search(event) {
    if (event.key === 'Enter') {
      if (event.target.value !== "") {
        console.log("i is " + i);
        HTMLElement.prototype.remove = function () { this.parentNode.removeChild(this); return this; }
        var x = document.createElement("TEXTAREA");
        document.body.appendChild(x);
        document.getElementsByTagName("TEXTAREA")[i].setAttribute("cols", "20");
        document.getElementsByTagName("TEXTAREA")[i].setAttribute("rows", "8");
        document.getElementsByTagName("TEXTAREA")[i].setAttribute("class", i);
        document.getElementsByTagName("TEXTAREA")[i].setAttribute("id", "area");
        document.getElementsByTagName("TEXTAREA")[i].setAttribute("style", "margin-left: 140px; margin-bottom :20px; margin-top: 20px;min-height: 60px;overflow-y: auto;word-wrap:break-word;")
        var button = document.createElement("button")
        document.body.appendChild(button);
        document.getElementsByTagName("button")[i + 1].setAttribute("type", "button");
        document.getElementsByTagName("button")[i + 1].setAttribute("class", i);
        document.getElementsByTagName("button")[i + 1].setAttribute("id", "btn");
        list.push({ id: i, description: event.target.value, dateTime: Date.parse(new Date().toLocaleDateString() + " " + new Date().toLocaleTimeString()) });
        document.getElementsByTagName("TEXTAREA")[i].value = list[i].description;

        console.log("list " + JSON.stringify(list))

        event.currentTarget.value = "";
        i++;
      }
      else {
        alert("Please Enter some text to save")
      }
    }
  }
  render() {
    return (
      <div className="flexContainer">
        <Widget search={this.search.bind(this)}></Widget>
        <SortButton></SortButton>
      </div>
    );
  }
}
const backgroundColor = ["yellow", "pink", "lightblue", "lightgreen", "lightcoral", "lightsalmon", "lightskyblue", "lightyellow"][Math.floor(Math.random() * 8)]

const Widget = (props) =>
  <input style={{ backgroundColor }} type="text" id="inp" className="inputField" placeholder="Take a note" onKeyDown={props.search} />

const SortButton = (props) =>
  <button className="but" style={{ height: "33px", padding: "0px 30px", margin: "8px 0" }}>Sort</button>

export default App;



// class App extends Component {
//   render() {
//     const fontSize = Math.floor(Math.random()*80) + 20;
//     const backgroundColor = ['red', 'green', 'blue', 'black'][Math.floor(Math.random()*4)];
//     const backgroundColor1 = ['red', 'green', 'blue', 'black', 'orange'][Math.floor(Math.random()*5)];
//     return(
//       <div>
//       <div style = {{
//         fontSize : `${fontSize}px`,
//         backgroundColor : backgroundColor1,
//         color : "white",
//         display : "flex",
//         justifyContent : "center",
//         alignItems : "center",
//         height : '200px',
//         width : '500px',
//         float : "right"
//       }}>
//       Hello World!
//       </div>
//       <div  style = {{
//         fontSize : `${fontSize}px`,
//         backgroundColor,
//         color : "white",
//         display : "flex",
//         justifyContent : "center",
//         alignItems : "center",
//         height : '200px',
//         width : '500px'
//       }}>Props : {this.props.txt}</div>
//       </div>
//     )
//   }
// }
// class App extends React.Component {
//   render(){
//     return <Button> I <Hearts /> React </Button>
//   }
// }
//
// const Button = (props) => <button> {props.children} </button>
//
// class Hearts extends React.Component {
//   render(){
//     return <span> &spades; </span>
//   }
// }
//   constructor(){
//     super();
//
//     this.state = {
//       texts : 'this is the state texts',
//       cat : 5
//     }
//   }
//
//   update( e ){
//     this.setState({texts : e.target.value})
//   }
//   render(){
//     return (
//       <div>
//       <input type ="text"
//         onChange={this.update.bind(this)} />
//         <h1> {this.state.texts} : {this.state.cat} </h1>
//       </div>
//     );
//     // return React.createElement('h1', null, "Hello World method 1!")
//   }
// }

//stateless function can't have state while class Component can have!
// const App = () => <h1> Hello World method 2! </h1>

// App.propTypes = {
//   texts : PropTypes.string,
//   cat : PropTypes.number.isRequired
// }
//
// App.defaultProps = {
//   texts: "this is the default txt"
// };

